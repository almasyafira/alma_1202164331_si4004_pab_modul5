package com.example.android.articleapps;

public class Article {
    String author;
    String title;
    String desc;
    String created_at;

    public Article(String author, String title, String desc, String created_at) {
        this.author = author;
        this.title = title;
        this.desc = desc;
        this.created_at = created_at;
    }

    public String getPenulis() {
        return author;
    }

    public void setPenulis(String author) {
        this.author = author;
    }

    public String getJudul() {
        return title;
    }

    public void setJudul(String title) {
        this.title = title;
    }

    public String getDeskripsi() {
        return desc;
    }

    public void setDeskripsi(String desc) {
        this.desc = desc;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getCreated_at() {
        return created_at;
    }
}
