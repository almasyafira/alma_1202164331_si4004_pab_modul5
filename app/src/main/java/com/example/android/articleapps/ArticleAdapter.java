package com.example.android.articleapps;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ArticleHolder> {

    private Context context;
    private List<Article> list;

    public ArticleAdapter(Context context, List<Article> mList) {
        this.context = context;
        this.list = mList;
    }

    @NonNull
    @Override
    public ArticleHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ArticleHolder(LayoutInflater.from(context).inflate(R.layout.item_article, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleAdapter.ArticleHolder holder, int position) {
        final Article data = list.get(position);

        holder.txtpublisher.setText(data.author);
        holder.txttitle.setText(data.title);
        holder.txtdesc.setText(data.desc);

        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Share Intent: " + data.title, Toast.LENGTH_SHORT).show();
            }
        });

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context, DetailArticleActivity.class);
                i.putExtra("penulis", data.author + ", " + data.created_at);
                i.putExtra("judul", data.author);
                i.putExtra("deskripsi", data.desc);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public Article getDataArticle(int position){
        return list.get(position);
    }

    class ArticleHolder extends RecyclerView.ViewHolder {

        private TextView txtpublisher, txttitle, txtdesc;
        private ImageView share;
        private CardView cardView;

        ArticleHolder(@NonNull View itemView) {
            super(itemView);

            txtpublisher = itemView.findViewById(R.id.txtpublisher);
            txttitle = itemView.findViewById(R.id.txt_tittle);
            txtdesc = itemView.findViewById(R.id.txt_desc);
            share = itemView.findViewById(R.id.img_share_item);
            cardView = itemView.findViewById(R.id.card_item);
        }
    }
}

