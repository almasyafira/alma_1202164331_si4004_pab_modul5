package com.example.android.articleapps;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class ListArticleActivity extends AppCompatActivity {

    RecyclerView rView;
    ArticleAdapter articleAdapter;
    List<Article> Listarticle;
    DbHelper dbHelper;

    final String PREF_NIGHT_MODE = "NightMode";
    SharedPreferences spNight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.DarkTheme);
        }else{
            setTheme(R.style.AppTheme);

            spNight = getSharedPreferences(PREF_NIGHT_MODE , Context.MODE_PRIVATE);
            if(spNight.getBoolean(PREF_NIGHT_MODE,false)){
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_article);

        dbHelper = new DbHelper(this);
        rView = findViewById(R.id.recycle_article);
        rView.setLayoutManager(new LinearLayoutManager(this));
        Listarticle = new ArrayList<>();

        dbHelper.readData(Listarticle);

        articleAdapter = new ArticleAdapter(this, Listarticle);
        rView.setAdapter(articleAdapter);

        articleAdapter.notifyDataSetChanged();
    }
}

