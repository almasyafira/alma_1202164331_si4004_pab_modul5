package com.example.android.articleapps;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class DetailArticleActivity extends AppCompatActivity {

    TextView txtdauthor, txtdtitle, txtddesc, dnotif;
    final String PREF_FONT_SIZE = "BigSize";
    SharedPreferences spFont;

    final String PREF_NIGHT_MODE = "NightMode";
    SharedPreferences spNight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.DarkTheme);
        }else{
            setTheme(R.style.AppTheme);

            spNight = getSharedPreferences(PREF_NIGHT_MODE , Context.MODE_PRIVATE);
            if(spNight.getBoolean(PREF_NIGHT_MODE,false)){
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_article);

        txtdauthor = findViewById(R.id.txtauthor);
        txtddesc = findViewById(R.id.txtdesc);
        txtdtitle = findViewById(R.id.txtTittle);
        dnotif = findViewById(R.id.txtnotif);

        if (getIntent() != null){
            txtdauthor.setText(getIntent().getStringExtra("penulis"));
            txtddesc.setText(getIntent().getStringExtra("deskripsi"));
            txtdtitle.setText(getIntent().getStringExtra("judul"));
        }

        spFont = getSharedPreferences(PREF_FONT_SIZE, Context.MODE_PRIVATE);
        if (spFont.getBoolean(PREF_FONT_SIZE, false)){
            txtddesc.setTextSize(22);
            dnotif.setVisibility(View.VISIBLE);
        }else{
            dnotif.setVisibility(View.INVISIBLE);
            txtddesc.setTextSize(16);
        }
    }
}

