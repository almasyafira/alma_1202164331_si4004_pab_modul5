package com.example.android.articleapps;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

public class Setting extends AppCompatActivity {

    Button btnsave;
    Switch swNight, swFont;
    SharedPreferences spFont, spNight;

    final String PREF_NIGHT_MODE = "NightMode";
    final String PREF_FONT_SIZE = "BigSize";

    SharedPreferences.Editor editNight, editFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.DarkTheme);
        }else{
            setTheme(R.style.AppTheme);

            spNight = getSharedPreferences(PREF_NIGHT_MODE , Context.MODE_PRIVATE);
            if(spNight.getBoolean(PREF_NIGHT_MODE,false)){
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        btnsave = findViewById(R.id.btn_save_setting);
        swNight = findViewById(R.id.swnight);
        swFont = findViewById(R.id.swfont);

        spFont = getSharedPreferences(PREF_FONT_SIZE, Context.MODE_PRIVATE);

        if (spFont.getBoolean(PREF_FONT_SIZE, false)){
            swFont.setChecked(true);
        }

        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveConfig();
            }
        });

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            swNight.setChecked(true);
        }
        swNight.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                }else{
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                }
            }
        });
    }

    private void saveConfig() {
        if (swFont.isChecked()){
            editFont = spFont.edit();
            editFont.putBoolean(PREF_FONT_SIZE, true);
            editFont.apply();
        }

        if (swNight.isChecked()){
            editNight = spNight.edit();
            editNight.putBoolean(PREF_NIGHT_MODE, true);
            editNight.apply();
        }

        Intent i = new Intent(Setting.this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }
}
