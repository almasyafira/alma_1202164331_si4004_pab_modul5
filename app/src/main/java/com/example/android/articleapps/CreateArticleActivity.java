package com.example.android.articleapps;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CreateArticleActivity extends AppCompatActivity {

    EditText edttitle, edtdesc, edtauthor;
    Button btnpost;
    String sTitle, sArticle, sAuthor;
    private DbHelper db;

    final String PREF_NIGHT_MODE = "NightMode";
    SharedPreferences spNight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.DarkTheme);
        }else{
            setTheme(R.style.AppTheme);

            spNight = getSharedPreferences(PREF_NIGHT_MODE , Context.MODE_PRIVATE);
            if(spNight.getBoolean(PREF_NIGHT_MODE,false)){
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_article);

        db = new DbHelper(this);
        edttitle = findViewById(R.id.txtTittle);
        edtdesc = findViewById(R.id.txtdesc);
        edtauthor = findViewById(R.id.txtAuthor);
        btnpost = findViewById(R.id.btnpost);

        btnpost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postArticle();
            }
        });

    }

    private void postArticle() {
        sTitle = edttitle.getText().toString().trim();
        sArticle = edtdesc.getText().toString().trim();
        sAuthor = edtauthor.getText().toString().trim();

        if (sTitle.isEmpty()){
            msg("Title kosong");
            return;
        }
        if (sArticle.isEmpty()){
            msg("Article kosong");
            return;
        }
        if (sAuthor.isEmpty()){
            msg("Author kosong");
            return;
        }

        saveToDb();

    }

    private void saveToDb() {
        if (db.insertData(new Article(sAuthor, sTitle, sArticle, getDateTime()))){
            msg("Berhasil ditambahkan");
            startActivity(new Intent(CreateArticleActivity.this, ListArticleActivity.class));
        }else {
            msg("Terjadi kesalahan");
        }
    }

    private void msg(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "EEE d MMM `yy", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }
}

